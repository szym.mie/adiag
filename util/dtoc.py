#!/usr/bin/python

import sys, re, csv, json
from functools import reduce


def prog_exit(c, s):
    """ prints error message 's' to STDERR and exits with 'c' status. """
    print(s, file=sys.stderr)
    sys.exit(c)

def prog_out(f, d):
    try:
        # create writer.
        w = csv.DictWriter(f, ("name", "id", "agh", "wght"))
        # write header.
        w.writeheader()
        # write rows.
        w.writerows(d)
    except (PermissionError, FileExistsError):
        prog_exit(1, 'Could not open output file.')

def main():
    """ main function. """
    try:
        _, *param, inn = sys.argv
    except ValueError:
        prog_exit(1, "Invalid or unreadable arguments.\n\nUsage:\n\ndattocsv.py [-i] <input> <output>\n\nOptions:\n\n-i   ignore any unreadable authors that are not root author.\n\n")

    # remove any dashes in front of option letters.
    try:
        param = reduce(lambda x, s: s+x, map(lambda s: s.strip("-"), param))
    except TypeError:
        param = ""

    # a Regex to match horizontal divider of table.
    c = re.compile("^(-+[+]){3}-+$")
    d = []
    r = None
    o = None
    try:
        with open(inn, "r", encoding="utf-8") as ins:
            if "j" in param:
                # JSON parser.
                def _ids_count_(a, i):
                    for n in {0, 1}:
                        try:
                            a[i["ids"][n]]+=1
                        except KeyError:
                            a[i["ids"][n]]=1
                    return a

                def _find_id_(n, i):
                    for d, e in enumerate(n):
                        if i == e["id"]:
                            return d
                    return None

                def _find_ids_(l, i):
                    for d, e in enumerate(l):
                        if i in e["ids"]:
                            return d
                    return None
                
                # load json
                j = json.load(ins)

                # get count of link ids, so root can be found.
                c = reduce(_ids_count_, j["links"], {})

                # get length of links.
                l = len(j["links"])
                for k, v in c.items():
                    # if someone has the same occurrence.
                    if v == l:
                        # find the node.
                        i = _find_id_(j["nodes"], k)
                        # else exit.
                        if i == None: prog_exit(2, "Cannot find root author entry.")
                        # get root.
                        r = j["nodes"][i]
                        # parse boolean.
                        r["agh"] = "true" if r["agh"] else "false"
                        # root gets weight 0.
                        r["wght"] = 0
                        # CSV does not hold acc.
                        del r["acc"]
                        # select rest of authors.
                        o = j["nodes"][0:i]+j["nodes"][i+1:len(j["nodes"])]
                        for e in o: 
                            e["agh"] = "true" if e["agh"] else "false"
                            e["wght"] = j["links"][_find_ids_(j["links"], e["id"])]["wght"]
                            del e["acc"]
                        # add everone to list.
                        d.append(r)
                        d.extend(o)

            else:
                # DAT parser
                def _root_entry_(n, a=True):
                    c = re.compile("^Autor: (\S+ \S+), (\d+)$")
                    try:
                        l = c.findall(n)[0]
                        return {
                            "name": l[0],
                            "id": l[1],
                            "agh": "true" if a else "false",
                            "wght": "0"
                        }
                    except IndexError:
                        return None


                def _separate_entry_(n):
                    try:
                        l = tuple(map(lambda s: s.strip(), n.split("|")))
                        return {
                            "name": l[0],
                            "id": l[1],
                            "agh": "true" if l[2] == "t" else "false",
                            "wght": l[3]
                        }
                    except IndexError:
                        return None
                
                # get first line data: root author, ID.
                r = _root_entry_(ins.readline(), "f" not in param)

                # if first author cannot be found terminate with 1.
                if r != None: d.append(r)
                else: prog_exit(2, "Cannot find root author entry.")

                # go to the table beginning.
                while not c.match(ins.readline()): pass

                # get every other author.
                o = tuple(map(_separate_entry_, ins.readlines()))

                if "i" in param:
                    # ignores entries that cannot be processed.
                    d.extend(tuple(filter(lambda n: n is not None, o)))
                else:
                    # if any entry is malformed terminates with 2.
                    if o != tuple(filter(lambda n: n is None, o)): d.extend(o)
                    else: prog_exit(3, "Authors table is malformed.")
    except (FileNotFoundError, PermissionError):
        prog_exit(1, 'Could not open input file.')

    if "n" in param:
        with open(f"{r['id']}.csv", "w", encoding="utf-8", newline="") as f:
            prog_out(f, d)
    else:
        prog_out(sys.stdout, d)
    
    sys.exit(0)

if __name__ == "__main__":
    main()
# BPPvis

A bibliography of publications visualisation for BPP AGH.

## Using BPPvis

The site itself does not require any other dependencies - however to ensure that it is possible to fetch data (and avoid CORS), this project should be served via server, rather than simply opening *index.html* in a browser.

Therefore *BPPvis* could be effortlessly run on any kind of HTTP server.

However, when accessing the site by default URL, one will be confronted with a message, warning that the ID was not selected.

To show a diagram it is mandatory to pass **id** URL parameter, with the selected ID. For example, with local server, serving on port 3000:

    https://localhost:3000/index.html?id=3000

This will fetch a diagram for a person associated with ID 3000.
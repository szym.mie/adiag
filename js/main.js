// DIMENSIONS of the arc diagram.
const DIMENSIONS = {width: 700, height: 900};
DIMENSIONS.width = DIMENSIONS.height = Math.max(window.innerWidth, window.innerHeight);

const DATA_VIEW_ROOT = "#data-view";
const WAIT_SCREEN_ROOT = "#wait-screen";
const MAIN_AUTHOR = "#main-author";

// need trailing slash
const BPP_SITE_AUTHOR_URL = "https://bpp.agh.edu.pl/autor/";
const BPP_API_URL = "https://api.bpp.agh.edu.pl/partnership/";

const ACC_COLOR = "#eb4034"; // Accent
const AMB_COLOR = "#888"; // Ambient
const NORM_COLOR = "#101010"; // Normal
const OTHER_COLOR = "#1756a3"; // Other
const SEL_COLOR = "#1f3652"; // Selected
const NOSEL_COLOR = "#818d9c80"; // Non-selected
const ERROR_COLOR = "#b22"; // Error
const STROKE_COLOR = "#000000ff" // Stroke


const TICK_SCALE = [1, 2, 5, 10, 25]; // scale for tick and tick labels.


// creating svg element and root group.
const svg = d3.select(DATA_VIEW_ROOT)
    // add svg to data-view.
    .append("svg")
    .append("g") // add primary group.
        .attr("transform", `translate(${DIMENSIONS.width/2},${DIMENSIONS.height/2})`); // add translation.

// add tip.
const tip = d3.select(DATA_VIEW_ROOT)
    .append("div")
    .style("visibility", "hidden") // initially hidden.
    .attr("class", "tip")
    .style("background-color", "white")
    .style("border-style", "solid")
    .style("border-width", "1px")
    .style("border-radius", "5px")
    .style("padding", "10px");

let cache; // cached data here.
let group;
let draw_cb = createChordDiagram; // function to call

// draw, using draw callback, and recover result..
const draw = () => {
    group = draw_cb(cache);
}

// clear svg - remove all of its elements.
const clear = () => {
    d3.select(DATA_VIEW_ROOT)
        .selectChild("svg")
        .selectChild("g")
        .selectAll("*").remove();
}


// 1111
// 2000
// fetch real data.
const params = parseParams();
// if not present warn about missing ID.
if (params.id === undefined) 
    showWaitError("No ID selected.")
else { 
    switch (params.df === undefined ? null : params.df[0]) {
        case "csv":
            // gather all promises - data is fetched asynchronously.
            Promise.all(params.id.map(v => d3.csv(`${BPP_API_URL}${v}`))).then(res => {
                // cache data for redraw.
                cache = dataMerge(...res.map( // perform data merge,
                    v => csvDecode(v) // after decoding every CSV file.
                    ));
                // trigger draw.
                updateDiagram();
                // update heading.
                updateHeader();
                // hide wait screen.
                hideWaitScreen();
            }).catch(err => {showWaitError(err)});
            break;
        case "json":
        default:
            // just select the first ID.
            params.id = [params.id[0]];
            d3.json(`${BPP_API_URL}${params.id}`).then(res => {
                // cache response.
                cache = res;
                // update diagram.
                updateDiagram();
                // update header.
                updateHeader();
                // hide wait screen.
                hideWaitScreen();
            }).catch(err => {showWaitError(err)})
            break;
    }
}

const clamp = (a, min, max) => Math.min(Math.max(a, min), max);

// parse URL parameters.
function parseParams() {
    const params = {};
    window.location
        .search
        .substr(1)
        .split("&")
        .map(param => {
            const [name, value] = param.split("=");
            try {
                params[name] = value.split(",");
            } catch(e) {
                params[name] = undefined;
            }
        });
    return params;
}

// update header information.
function updateHeader() {
    const ids = params.id === undefined ? [] : params.id.map(v => parseInt(v));
    console.log(ids)
    const names = cache.nodes.filter(v => ids.includes(v.id)).map(v => v.name);
    const ma = d3.select(MAIN_AUTHOR)
    ids.map((c, i) => {
        ma
            .append("a")
            .attr("href", `${BPP_SITE_AUTHOR_URL}${c}`)
            .text(`${names[i]}`);
    });
}

// update the svg elem.
function updateSVG() {
    svg
        .attr("transform", `translate(${DIMENSIONS.width/2},${DIMENSIONS.height/2})`); // add translation.
}

// update dimensions of viewport.
function updateDimensions() {
    const s = d3.select(DATA_VIEW_ROOT).selectChild("svg").node();
    DIMENSIONS.width = s.clientWidth;
    DIMENSIONS.height = s.clientHeight;
}

// does some functions in correct order.
function updateDiagram() {
    updateDimensions();
    updateSVG();
    clear();
    draw();
}

// show wait screen.
function showWaitScreen() {
    d3.select(WAIT_SCREEN_ROOT)
        .style("opacity", 1)
        .style("display", "flex");
}

// hide the wait screen.
function hideWaitScreen() {
    const ws = d3.select(WAIT_SCREEN_ROOT);
    ws.style("opacity", 0);
    d3.timeout(() => {ws.style("display", "none")}, 500);
}

// interrupt and show an error message.
function showWaitError(err="An error occurred.") {
    const ws = d3.select(WAIT_SCREEN_ROOT);
    // stop circles and make them red.
    ws.selectChildren(".wait-screen-circle")
        .style("border-color", ERROR_COLOR)
        .style("background-color", ERROR_COLOR)
        .style("animation-iteration-count", "0");
    // add message.
    ws.append("p")
        .style("color", ERROR_COLOR)
        .style("margin-left", "10px")
        .text(err);
}

// resume and clear an error message.
function clearWaitError(err) {
    const ws = d3.select(WAIT_SCREEN_ROOT);
    // resume animation of the circles.
    ws.selectChildren(".wait-screen-circle")
        .style("border-color", AMB_COLOR)
        .style("background-color", AMB_COLOR)
        .style("animation-iteration-count", "infinite");
    // remove paragraph.
    ws.selectChildren("p").remove();
}

// fetch CSV IDs.
function *csvFetch(data) {
    for ({id: id} of data) yield `${id}`;
}

// decode CSV data into JSON.
function csvDecode(data) {
    // result.
    out = {
        nodes: [],
        links: []
    };
    // get root id.
    root_id = parseInt(data[0].id);
    // iterate CSV lines.
    for (const {name, id, agh, wght} of data) {
        out.nodes.push({
            name: name, // pass name.
            id: parseInt(id), // make name an integer.
            agh: (agh=="true" || agh=="1") // replacing 'false' and 'true' with '0' and '1' respectively is allowed.
        });
        // prevent pushing link to itself.
        if (root_id != id)
            out.links.push({
                ids: [root_id, parseInt(id)], 
                wght: parseInt(wght)
            });
    }
    // first author should be accented.
    out.nodes[0].acc = true;
    return out;
}

// perform data merge of JS objects.
function dataMerge(...data) {
    // Result.
    out = {
        nodes: [],
        links: []
    };
    // Observe used nodes, links to prevent duplicates.
    const used_nodes = [], used_links = [];
    // iterate thorough JS objects.
    for (const {nodes: nodes, links: links} of data) {
        // nodes iteration.
        for (const node of nodes) {
            // get node ID.
            const nid = node.id;
            // if this node is accented, search for used nodes, and accent it.
            if (node.acc) {
                fi = used_nodes.findIndex(v => v === nid); // if already in used nodes,
                if (fi !== -1) out.nodes[fi].acc = true; // make sure it gets accented.
            };
            // if used nodes doesn't include it yet push node out.
            if (!used_nodes.includes(nid)) {
                out.nodes.push(node);
                used_nodes.push(nid);
            }
        }
        // links iteration.
        for (const link of links) {
            // get individual links IDs.
            const [lid0, lid1] = link.ids;
            // construct combinations: assuming IDs are at most 32-bits wide.
            const cmb0 = lid1<<32 | lid0, cmb1 = lid0<<32 | lid1;
            // if does not include, then append the link.
            if (!used_links.includes(cmb0) && !used_links.includes(cmb1)) {
                out.links.push(link);
                used_links.push(cmb0);
            }
        }
    }
    return out;
}

function *colorWheelGenerator(length, lig=255, sat=255, alp=255) {
    for (let i = 0; i < length; i++) {
        // hopefully this will in some way make diagram colors more distinct.

        const rad = i;
        const off = 2*Math.PI/3;

        // calculate colors.
        let r = Math.abs(Math.sin(rad))*255;
        let g = Math.abs(Math.sin(rad+off)*255);
        let b = Math.abs(Math.sin(rad+2*off)*255);

        const lig_nrm = lig / Math.max(r, g, b);

        // darkening.
        r*=lig_nrm;
        g*=lig_nrm;
        b*=lig_nrm;

        const sat_avg = (r + g + b) / 3;
        const sprop = sat / 255;
        const iprop = 1 - sprop;
        const sacol = sat_avg*iprop;

        // saturate.
        r = r*sprop + sacol;
        g = g*sprop + sacol;
        b = b*sprop + sacol;
        
        // clamping.
        r = r > 255 ? 255 : r;
        g = g > 255 ? 255 : g;
        b = b > 255 ? 255 : b;

        // stringify.
        const rs = parseInt(r).toString(16).padStart(2, "0");
        const gs = parseInt(g).toString(16).padStart(2, "0");
        const bs = parseInt(b).toString(16).padStart(2, "0");
        const as = alp.toString(16).padStart(2, "0");
        yield `#${rs}${gs}${bs}${as}`;
    }
}

// create a horizontal diagram.
function createHorizontalDiagram(data) { 

    // getting nodes names from JSON.
    const nodes_names = data.nodes.map(d => d.name);

    // creating linear scale.
    const x_scale = d3.scalePoint()
        .range([0, DIMENSIONS.width])
        .domain(nodes_names);


    // svg
    //     .selectAll("nodes")
    //     .data(data.nodes)
    //     .enter()
    //     .append("circle")
    //         .attr("cx", d => x_scale(d.name))
    //         .attr("cy", DIMENSIONS.height - 30)
    //         .attr("r", 8)
    //         .style("fill", "#a8532d");

    const d_labels = svg
        .selectAll("labels")
        .data(data.nodes)
        .enter()
        .append("g")
            .attr("transform", d => `rotate(-45, ${x_scale(d.name)}, ${DIMENSIONS.height-10})`)
        .append("text")
            .attr("x", d => x_scale(d.name))
            .attr("y", DIMENSIONS.height - 10)
            .text(d => d.name)
            .style("text-anchor", "end")
            .style("fill", d => (d.agh ? NORM_COLOR : OTHER_COLOR));

    const id_nodes = {};
    for (const o of data.nodes) id_nodes[o.id] = o;

    const d_links = svg
        .selectAll("links")
        .data(data.links)
        .enter()
        .append("path")
        .attr("d", d => {
            const start = x_scale(id_nodes[d.ids[0]].name)
            const end = x_scale(id_nodes[d.ids[1]].name)
            return [
                `M ${start} ${DIMENSIONS.height - 30}`,
                `A ${(start - end) / 2}, ${(start - end) / 2}, 0, 0, ${start < end ? 1 : 0} ${end}, ${DIMENSIONS.height - 30}`
            ].join(" ")
        })
        .style("fill", "none")
        .attr("stroke", STROKE_COLOR)
        .attr("stroke-width", d => d.wght/2);
}

// create a vertical diagram.
function createVerticalDiagram(data) { 

    // getting nodes names from JS object.
    const nodes_names = data.nodes.map(d => d.name);

    // creating linear Y scale;
    const y_scale = d3.scalePoint()
        .range([0, DIMENSIONS.height]) // with a range of maximum height excluding margins,
        .domain(nodes_names); // and setting domain to nodes names.


    // you can draw blobs, if you wish to.
    // svg
    //     .selectAll("nodes")
    //     .data(data.nodes)
    //     .enter()
    //     .append("circle")
    //         .attr("cx", d => x_scale(d.name))
    //         .attr("cy", DIMENSIONS.height - 30)
    //         .attr("r", 8)
    //         .style("fill", "#a8532d");

    // drawing labels here.
    const d_labels = svg
        .selectAll("labels")
        .data(data.nodes) // get data for nodes.
        .enter()
        .append("text") // add text.
            //.attr("transform", d => `rotate(-45, ${x_scale(d.name)}, ${DIMENSIONS.height-10})`)
            .attr("x", DIMENSIONS.left) // move to the left margin.
            .attr("y", d => y_scale(d.name)) // position according to y_scale.
            .text(d => d.name) // set the according text.
            .style("text-anchor", "end") // anchor it by the end.
            .style("dominant-baseline", "middle")
            .style("font-size", "4")
            .style("fill", d => {
                if (d.acc) return ACC_COLOR; // if accented
                if (d.agh) return NORM_COLOR; // if AGH
                return OTHER_COLOR; // else other.
            });

    // resolving links array indices.
    const id_nodes = {};
    for (const o of data.nodes) id_nodes[o.id] = o;

    // drawing links here.
    const d_links = svg
        .selectAll("links")
        .data(data.links) // grab data for links.
        .enter()
        .append("path") // add path.
            .attr("d", d => {
                const start = y_scale(id_nodes[d.ids[0]].name) // get start Y coordinate.
                const end = y_scale(id_nodes[d.ids[1]].name) // get end Y coordinate.
                let size = (end-start)/2 < DIMENSIONS.width ? // if deltaY lower then width of SVG;
                (end-start)/2 + DIMENSIONS.left+5 : // calculate handle for quadratic bezier.
                DIMENSIONS.width+DIMENSIONS.left; // set it to the most right coordinate possible.
                return [
                    `M ${DIMENSIONS.left+5} ${start}`, // move to start Y.
                    `Q ${size}, ${(start+end)/2}, ${DIMENSIONS.left+5}, ${end}` // draw quadratic bezier curve.
                ].join(" ") // return drawing commands for path.
            })
            .style("fill", "none")
            .attr("stroke", STROKE_COLOR)
            .attr("stroke-width", d => d.wght**.7); // set an exponential width of links.

    const showLinkTip = function() {
        // get data from event target.
        const d = d3.select(this).data()[0];
        // set every link to semi-transparent.
        d_links.style("stroke", NOSEL_COLOR);
        // make labels text with stroke.
        d_labels.style("stroke-width", "2px");
        // default stroke color is semi-transparent.
        d_labels.style("stroke", NOSEL_COLOR);
        // get 'end' labels for link.
        d_labels.filter(v => d.ids.includes(v.id)).style("stroke", SEL_COLOR);
        tip
            .style("visibility", "visible") // make tip visible.
            .html(`Publications: ${d.wght}`); // show number of publications.
        d3.select(this)
            .style("stroke-width", d => d.wght*1.5+10) // widen the link.
            .style("stroke", SEL_COLOR); // change it's color.
    };
    
    const hideLinkTip = function() {
        // set all links to default color.
        d_links.style("stroke", NORM_COLOR);
        // remove labels stroke.
        d_labels.style("stroke-width", 0);
        tip
            .style("visibility", "hidden"); // hide the tip.
        d3.select(this)
            .style("stroke-width", d => d.wght**.7) // return to default width.
            .style("stroke", NORM_COLOR); // use default color.
    };
    
    const showNodeTip = function() {
        // get data from event target.
        const d = d3.select(this).data()[0];
        // make every link semi-transparent.
        d_links.style("stroke", NOSEL_COLOR);
        // filter links that includes ID of selected node.
        const l = d_links.filter(v => v.ids.includes(d.id));
        // make them highlighted,
        l.style("stroke", SEL_COLOR);
        tip
            .style("visibility", "visible") // make tip visible.
            .html(`ID: ${d.id}<br>Total publications: ${l.data().map(v => v.wght).reduce((a,v) => a+v)}`);
    };
    
    const hideNodeTip = function() {
        // make all links normal again.
        d_links.style("stroke", NORM_COLOR);
        tip
            .style("visibility", "hidden"); // hide the tip.
    };
    
    const moveTip = function(ev) {
        tip
            .style("visibility", "visible") // make sure tip is visible.
            .style("left", `${ev.pageX+5}px`) // position tip.
            .style("top", `${ev.pageY+5}px`);
    };

    // setup labels event callbacks.
    d_labels.on("mouseover", showNodeTip);
    d_labels.on("mousemove", moveTip);
    d_labels.on("mouseout", hideNodeTip);

    // setup links event callbacks.
    d_links.on("mouseover", showLinkTip);
    d_links.on("mousemove", moveTip);
    d_links.on("mouseout", hideLinkTip);
}

function createChordDiagram(data) {
    // acquire shorter dimension.
    if (data.links.length === 0) {
        noContibText(data);
        return null;
    }

    // minimal dimension.
    const min_rad = Math.min(DIMENSIONS.width, DIMENSIONS.height);
    // sum of links.
    const link_sum = data.links.reduce((a, v) => a + v.wght, 0);

    // get text size based on shorter dimension and links sum.
    const text_size = clamp(Math.sqrt(25*min_rad/link_sum), 1, 12);

    // acquire distance between ticks.
    const tick_scale = selectTickScale(Math.sqrt(link_sum/min_rad)*3);

    // make stroke delicate.
    const stroke_width = clamp(Math.sqrt(min_rad/link_sum)*.5, 0.5, 2);

    // a so-called reality-adjusted radius.
    const real_rad = min_rad / (2.5+text_size/12*.5);

    // getting nodes names from JSON.
    const nodes_names = data.nodes.map(d => d.name);

    // resolving links array indices.
    const id_nodes = {};
    data.nodes.map(d => {id_nodes[d.id] = d});

    // getting ID order.
    const id_order = {};
    data.nodes.map((d, i) => {id_order[d.id] = i});

    const matrix = [];

    // filing matrix out with rows.
    const l = data.nodes.length;
    for (let i = 0; i < l; i++) matrix.push(Array(l).fill(0));

    for (const link of data.links) {
        const ind0 = id_order[link.ids[0]], ind1 = id_order[link.ids[1]];
        matrix[ind0][ind1] = link.wght;
        matrix[ind1][ind0] = link.wght;
    };

    // generating colors.
    const color = [...colorWheelGenerator(data.nodes.length, 320, 255, 210)];
    // caching them.
    cache.color = color;

    const res = d3.chord()
        .padAngle(clamp(min_rad/link_sum*0.005, 0.005, 0.025))
        .sortSubgroups(d3.descending)
        (matrix);

    svg
        .datum(res)
        .append("g")
        .selectAll("path")
        .data(d => d)
        .enter()
        .append("path")
            .attr("d", d3.ribbonArrow()
                .radius(real_rad)
            )
            .style("fill", d => color[d.target.index])
            .style("stroke", STROKE_COLOR)
            .style("stroke-width", stroke_width);
    
    const grp = svg
        .datum(res)
        .append("g")
        .selectAll("g")
        .data(d => d.groups)
        .enter();
    
    grp
        .append("g")
        .append("path")
            .style("fill", (_, i) => color[i])
            .style("stroke", STROKE_COLOR)
            .style("stroke-width", stroke_width)
            .attr("d", d3.arc()
                .innerRadius(real_rad + text_size/2)
                .outerRadius(real_rad + text_size)
            );
    
    grp
        .selectAll(".group-tick")
        .data(d => groupTicks(d, tick_scale))
        .enter()
        .append("g")
            .attr("transform", d => `rotate(${d.angle * 180 / Math.PI - 90}) translate(${real_rad+text_size}, 0)`)
        .append("line")
            .attr("x2", text_size/2)
            .attr("stroke", STROKE_COLOR)
            .attr("stroke-width", stroke_width)

    grp
        .selectAll(".group-tick-label")
        .data(d => groupTicks(d, tick_scale))
        .enter()
        .filter(d => d.value % tick_scale === 0)
        .append("g")
            .attr("transform", d => `rotate(${d.angle * 180 / Math.PI - 90}) translate(${real_rad+text_size}, 0)`)
        .append("text")
            .attr("x", 16)
            .attr("dy", 4)
            .attr("transform", d => d.angle > Math.PI ? "rotate(180) translate(-32)" : null)
            .style("text-anchor", d => d.angle > Math.PI ? "end" : null)
            .text(d => !d.value ? 
                `${d.angle > Math.PI ? "­­" : "↓"} ${data.nodes[d.data.index].name} ${d.angle > Math.PI ? "­­↑" : ""}`
                : d.value)
            .style("font-weight", d => data.nodes[d.data.index].acc ? "bold" : null)
            .style("font-style", d => !d.value ? "italic" : null)
            .style("text-decoration", d => !d.value && data.nodes[d.data.index].agh ? "underline" : null)
            .style("font-size", d => !d.value ? `${text_size}px` : `${text_size}px`);

    let inc_root_bool = false;
    document
        .getElementById("search_name")
        .addEventListener("input", 
            function () {
                restoreChordDiagram(); 
                searchChordDiagram(this.value, inc_root_bool);
        });
    document
        .getElementById("inc_root_switch")
        .addEventListener("click",
            function () {
                inc_root_bool = this.checked;
                document
                    .getElementById("search_name")
                    .dispatchEvent(new Event("input"));
        });

    return grp;
}

// perform search on text labels.
function searchChordDiagram(text, inc_root=false) {
    // if empty then do nothing.
    if (text === "") return;
    // create regular expression.
    const re = new RegExp(text);
    // acquire real indices where there is a match.
    const indices = cache.nodes.reduce((a,c,i) => {
        if (c.name.search(re) !== -1 && (inc_root || !c.acc)) a.push(i);
        return a;
    }, []);

    // if includes index.
    function includesIndex(i) {
        try {
            return indices.includes(i);
        } catch(e) {
            return false;
        }
    }

    // don't select main authors - it will highlight everyone.


    // select paths not on indices list.
    svg
        .selectAll("path")
        .filter(d => !(d.source === undefined 
            ? includesIndex(d.index) 
            : includesIndex(d.source.index) || includesIndex(d.target.index)))
        .style("fill", "#00000010")
        .style("stroke", "#00000050");
    // select ticks that are not on indices list.
    svg
        .selectAll("line")
        .filter(d => !indices.includes(d.data.index))
        .style("stroke", "#00000050");
}

// make default appearance of diagram.
function restoreChordDiagram() {
    // select all paths.
    svg
        .selectAll("path")
        .style("fill", d => d.target !== undefined ? cache.color[d.target.index] : cache.color[d.index])
        .style("stroke", STROKE_COLOR);
    // select all labels ticks.
    svg
        .selectAll("line")
        .style("stroke", STROKE_COLOR);
}

// if a person happens to have no publication then display this instead.
function noContibText(data, text=" has no publications.") {
    // add text.
    svg.append("text")
        .attr("x", 16)
        .attr("dy", 4)
        .style("text-anchor", "middle")
        .text(`${data.nodes[0].name}${text}`);
}

function groupTicks(d, step) {
    const k = (d.endAngle - d.startAngle) / d.value;
    return d3.range(0, d.value+1, step).map(v => ({data: d, value: v, angle: v * k + d.startAngle}));
}

function selectTickScale(d) {
    const i = TICK_SCALE.findIndex(v => v >= d);
    return TICK_SCALE[i];
}

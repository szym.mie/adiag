# Link combinations search

## Abstract

It is required for data merge to **not include duplicates**.
With nodes such test are quite easy and fast to carry out.

However matters complicate, when trying to do the same thing with links - we now have two values to compare. The order of values does not matter, just like in *mathematical set*.

## Solution

An algorithm is presented here to do this cleanly and in efficient manner.

> cmb<sub>0</sub> = link<sub>0</sub> << 32 | link<sub>1</sub>
>
> cmb<sub>1</sub> = link<sub>1</sub> << 32 | link<sub>0</sub>

Then it is tested if an array includes any of those combinations and either pushes the link in, or continue with other link to merge.

It is imperative, that **links IDs are at most 32-bit integers**. Otherwise collisions may occur (meaning that some links may not be appended, even thought they have not occurred once).s
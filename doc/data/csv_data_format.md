# CSV data format

## Abstract

***CSV file format*** has been chosen for **storing a singular author's links to other authors** - in other words the first author in the file becomes **root**, and every other author in the file becomes the **leaf**, directly connected to root.

In comparison to *JSON*, the structure is easier to understand, simpler, resulting in less data being fetched.

---

## Structure

Each entry stores:
- ### author's name
    type: *String*

    Full name of author.
- ### author's ID
    type: *Number*

    Stores an internal ID.
- ### is author AGH's affiliate
    type: *Boolean*

    Informs if the given author is AGH's affiliate.
- ### amount of publications
    type: *Number*

    Also known as **weight** - it is a property of link. That means, after the conversion to JSON format, this would become a property of *link* between *nodes*, rather than the *node* itself.

---

## Example

### With this abstract piece of data:

*Root* author:

**Name**: Anna Nowak, 

**ID**: 450,

**AGH?**: yes


| Name       | ID | AGH? | Publications |
|:----------:|:--:|:----:|:------------:|
|Jan Kowalski|679 |yes   |23            |

### The structure of CSV file on server would look like this:

    name,id,agh,wght
    Anna Nowak,450,true,0
    Jan Kowalski,679,true,23


Since first author is the *root*'s amount of publications (*wght*) is set to 0, to enhance readability. Nevertheless it can be any arbitrary integer.

*agh* property could be represented by *0* or *1*, as a replacement for *false* and *true* respectively.

**This data is then sent to the client. Any other actions happen on client side.**


### On client this CSV data would look like this, after conversion to JS object:

    {
        nodes: [
            {
                name: "Anna Nowak",
                id: 450,
                agh: true,
                isacc: true
            },
            {
                name: "Jan Kowalski",
                id: 679,
                agh: true
            }
        ],
        links: [
            {ids: [405, 679], wght: 23}
        ]
    }


As you may see, the first node got a special attribute **isacc** set to *true* - it tells that this node should be highlighted.

Links are resolved internally to match *nodes* array indices.

---

## Merging data

It is possible to get few CSV files, and then merge them together. Due to the nature of CSV and the above mentioned structure merging is done **after** conversions to JS objects.
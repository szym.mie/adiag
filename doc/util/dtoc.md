# dtoc

## Description

A small utility to quickly convert *.dat* or *json* files into a proper *.csv* data sets. The name stands for **D**ata**TOC**SV.

## Usage

    dtoc [-j] [-d] [-i] [-n] [-f] <file>

## Options

- *-i* ignores any malformed entries, except *root* author, without terminating.
- *-n* writes to file in current directory with the same filename as *root* author ID, requires write permission to do that.
- *-f* assumes that the main author is not an AGH affiliate.
- *-j* the input file should be treated as *JSON*.
- *-d* the input file should be treated as *DAT* (default).

## Output

Output is served to *stdout*, and with *-n* option to file with the same ID as main author (see above). Stream redirection or piping could be used to manipulate the data further (or save to file).

## Examples

1. Converting from *.dat* to *.csv*, printing to *stdout* (original):

        dtoc example.dat

2. Ignoring malformed entries, automatic writing to file:

        dtoc -ni 0001.dat

3. Supplying utility with *JSON*:

        dtoc -j 0002.json

## Status codes

- 0 -> success,
- 1 -> input (or output with *-n* option) file could not be opened,
- 2 -> *root* author entry is malformed,
- 3 -> some of author entries are malformed (one or more).

---